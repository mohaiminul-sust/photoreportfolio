<?php

use Illuminate\Database\Seeder;

class PortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolio')->insert([
            'site_title' => 'Ocularus',
            'banner_main_sub' => 'Let\'s explore!',
            'banner_main_title' => 'Ocularus\' Photography',
            'bg_image' => "/images/2.jpg",
            'banner_first_title' => 'Who am I?',
            'banner_first_desc' => 'Pellentesque non diam et tortor dignissim bibendum. Neque sit amet mauris egestas quis mattis velit fringilla. Curabitur viver justo sed scelerisque. Cras mattis consectetur purus sit amet.',
            'banner_second_title' => 'What do I do?',
            'banner_second_desc' => 'Tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia.',
            'intro_header' => 'Introduction',
            'intro_content' => 'Fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Etiam porta sem malesuada magna mollis euismod. Aenean lacinia bibendum nulla. Tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia. Pellentesque non diam et tortor dignissim bibendum. Neque sit amet mauris egestas quis mattis velit fringilla. Curabitur viver justo sed scelerisque. Cras mattis consectetur purus sit amet.',
            'address' => 'Road 5, Block F, Akhaliaghat, Sylhet-3100',
            'phone_number' => '+8801637907272',
            'user_id' => 1
        ]);
    }
}
