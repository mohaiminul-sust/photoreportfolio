<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePortfolioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolio', function (Blueprint $table) {
            $table->string('facebook_link')->nullable();
            $table->string('flickr_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('pexels_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolio', function (Blueprint $table) {
            $table->dropColumn('facebook_link');
            $table->dropColumn('flickr_link');
            $table->dropColumn('instagram_link');
            $table->dropColumn('pexels_link');            
        });
    }
}
