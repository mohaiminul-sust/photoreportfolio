<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_title');
            $table->string('banner_main_title');
            $table->string('banner_main_sub');
            $table->string('bg_image');
            $table->string('banner_first_title');
            $table->text('banner_first_desc');
            $table->string('banner_second_title');
            $table->text('banner_second_desc');
            $table->string('intro_header');
            $table->text('intro_content');
            $table->text('address');
            $table->string('phone_number');
            $table->timestamps();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio');
    }
}
