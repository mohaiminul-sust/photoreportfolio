<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'portfolio';

    protected $guarded = [];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
