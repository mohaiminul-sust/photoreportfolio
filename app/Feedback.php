<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';

    protected $guarded = [];

    public function scopeViewed($query)
    {
        $query->where(function ($query) {
            $query->where('viewed', 1);
        });
        return $query;
    }

    public function scopeNew($query)
    {
        $query->where(function ($query) {
            $query->where('viewed', 0);
        });
        return $query;
    }

    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword != '') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                      ->orWhere("email", "LIKE","%$keyword%");
            });
        }
        return $query;
    }
}
