<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use App\Http\Resources\PhotoResource;
use App\Http\Resources\SinglePhotoResource;

class PhotoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['getAllPublicPhotos']]);
    }

    public function index() {
        return view('photo.index')->with('title', 'Photos');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPhotos()
    {
        $photos = Photo::orderBy('created_at','desc')->paginate(15);
        return PhotoResource::collection($photos);
    }

    public function searchPhotos(Request $request) {
        $query = strtolower($request->input('query'));
        $photos = Photo::SearchByKeyword($query)->orderBy('created_at','desc')->paginate(15);
        return PhotoResource::collection($photos);
    }
    /**
     * Display the specified collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        // return $photo;
        return new SinglePhotoResource(Photo::find($id));
    }

    public function getPhotosByAlbum($id) {
        $photos = Photo::where('album_id', $id)->orderBy('created_at','desc')->paginate(10);
        return PhotoResource::collection($photos);
    }

    public function getAllPublicPhotos() {
        $photos = Photo::where('is_public', 1)->orderBy('created_at','desc')->get();
        return PhotoResource::collection($photos);
    }

    public function getPublicPhotos() {
        $photos = Photo::where('is_public', 1)->orderBy('created_at','desc')->paginate(10);
        return PhotoResource::collection($photos);
    }

    public function makePublic($id) {
        $photo = Photo::find($id);
        if($photo) {
            $photo->is_public = 1;
            $photo->save();
        }
        return response()->json(['Success, 200']);
    }

    public function getPrivatePhotos() {
        $photos = Photo::where('is_public', 0)->orderBy('created_at','desc')->paginate(6);
        return PhotoResource::collection($photos);
    }

    public function makePrivate($id) {
        $photo = Photo::find($id);
        if($photo) {
            $photo->is_public = 0;
            $photo->save();
        }
        return response()->json(['Success, 200']);
    }

    public function uploadimage() {
        return view('photo.upload')->with('title', 'Upload Photo');
    }

    public function uploadImageByAlbum($id) {
        return view('photo.uploadbyalbum')->with('id', $id)->with('title', 'Upload Photo');
    }

    public function store(Request $request, $id) {
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $path = '/uploads/albums/'.$id.'/photos/';
            $destinationPath = public_path().$path;

            $filename = $file->getClientOriginalName();
            // $namePart = bcrypt($filename).$file->getClientOriginalExtension();

            $file->move($destinationPath, $filename);

            $photo = new Photo();
            $photo->image = $path.$filename;
            $photo->album_id = $id;
            $photo->save();

            return new PhotoResource($photo);
        }
    }

    public function preview($id){
        return view('photo.preview')->with('id', $id)->with('title', 'Preview Photo');
    }

    public function edit($id) {
        return view('photo.edit')->with('id', $id)->with('title', 'Update Photo');
    }

    public function update(Request $request, $id) {
        $rules = [
            'caption' => 'required'
        ];

        $validator = \Validator::make($request->toArray(), $rules);
        if($validator->fails()){
            return \Redirect::route('photo.update', $id)
            ->withErrors($validator)
            ->withInput();
        }

        $photo = Photo::find($id);

        if ($photo) {
            $photo->caption = $request->caption;
            if ($request->has('notes')) {
                $photo->notes = $request->notes;
            }
            $photo->save();
        }
        
        flash('Photo updated!')->success();
        return redirect()->route('photo.preview', $id);
    }
    
    public function destroy($id) {
        $photo = Photo::find($id);
        if ($photo) {
            $path = public_path().$photo->image;
            if (file_exists($path)) {
                @unlink($path);
            }
            $photo->delete();
        }
        return response()->json(['Success, 200']);
    }

    public function updateImage(Request $request, $id) {
        $photo = Photo::find($id);

        if ($photo) {
            
            if ($photo->image) {
                $path = public_path().$photo->image;
                if (file_exists($path)) {
                    @unlink($path);
                }
            }

            if($request->hasFile('file'))
            {
                $file = $request->file('file');

                $destinationPath = public_path().'/uploads/albums/'.$id.'/photos/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);

                $photo->image = '/uploads/albums/'.$id.'/photos/'.$filename;
                $photo->save();
            }
        }

        return new PhotoResource($photo);
    }
}
