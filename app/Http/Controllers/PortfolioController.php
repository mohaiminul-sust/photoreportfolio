<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;
use App\Http\Resources\PortfolioResource;
use Auth;

class PortfolioController extends Controller
{
    /**
     * Display portfolio page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolio = Portfolio::find(1);
        return view('portfolio.index')->with('title', $portfolio->site_title);
    }

    /**
     * Return with portfolio content for user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getContent()
    {
        $portfolio = Portfolio::find(1);
        return new PortfolioResource($portfolio);
    }

    /**
     * Show the form for editing portfolio.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        $id = Auth::user()->portfolio->id;
        return view('portfolio.edit')->with('id', $id)->with('title', 'Update Portfolio');
    }

    /**
     * Update the specified portfolio in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $rules = [
            'siteName' => 'required',
            'mainTitle' => 'required',
            'mainSub' => 'required',
            'firstTitle' => 'required',
            'firstDesc' => 'required',
            'secondTitle' => 'required',
            'secondDesc' => 'required',
            'introTitle' => 'required',
            'introContent' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ];

        // return $rules;
        $validator = \Validator::make($request->toArray(), $rules);
        if($validator->fails()){
            return redirect()->route('portfolio.edit')
            ->withErrors($validator)
            ->withInput();
        }

        $portfolio = Portfolio::find($id);

        if ($portfolio) {
            $portfolio->site_title = $request->siteName;
            $portfolio->banner_main_title = $request->mainTitle;
            $portfolio->banner_main_sub = $request->mainSub;
            $portfolio->banner_first_title = $request->firstTitle;
            $portfolio->banner_first_desc = $request->firstDesc;
            $portfolio->banner_second_title = $request->secondTitle;
            $portfolio->banner_second_desc = $request->secondDesc;
            $portfolio->intro_header = $request->introTitle;
            $portfolio->intro_content = $request->introContent;
            $portfolio->address = $request->address;
            $portfolio->phone_number = $request->phone;
            $portfolio->facebook_link = $request->facebook_link;
            $portfolio->flickr_link = $request->flickr_link;
            $portfolio->instagram_link = $request->instagram_link;
            $portfolio->pexels_link = $request->pexels_link;
            $portfolio->bio = $request->bio;
            // if ($request->bio) {
            // }

            $portfolio->save();
        }
        
        flash('Portfolio updated!')->success();
        return redirect()->route('portfolio.edit');
    }

    public function updateBackground(Request $request, $id) {
        $portfolio = Portfolio::find($id);

        if ($portfolio) {

            if ($portfolio->bg_image) {
                $path = public_path().$portfolio->bg_image;
                if (file_exists($path)) {
                    @unlink($path);
                }
            }

            if ($request->hasFile('file'))
            {
                $file = $request->file('file');

                $destinationPath = public_path().'/uploads/portfolio/'.$id.'/background/';
                $filename = $file->getClientOriginalName();
                $file->move($destinationPath, $filename);

                $portfolio->bg_image = '/uploads/portfolio/'.$id.'/background/'.$filename;
                $portfolio->save();
            }
        }

        return new PortfolioResource($portfolio);
    }
}
