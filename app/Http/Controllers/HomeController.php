<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;
use Auth;
use App\Album as Album;
use App\Photo as Photo;
use App\Feedback as Feedback;
use Carbon\Carbon;
use Hash;
use App\Mail\PasswordMail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user update info form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showUpdateForm(){
        return view('auth.updateinfo');
    }

    /**
     * Update user info.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        Mail::to($user->email)->send(new PasswordMail($user, $request->get('new-password')));
 
        return redirect('home')->with("success","Password changed successfully !");
    }
        
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')
        ->with('adminUser', Auth::user())
        ->with('albumsCount', Album::count())
        ->with('photosCount', Photo::count())
        ->with('feedbacksCount', Feedback::count())
        ->with('newFeedbackCount', Feedback::where('viewed', 0)->count())
        ->with('title', 'Home');
    }

    public function albumTimeline() {
        return view('timeline.album')->with('title', 'Timeline');
    }

    public function photoTimeline() {
        return view('timeline.photo')->with('title', 'Timeline');
    }

    public function getTimelineAlbums() {
        $albums = Album::orderBy('created_at','desc')->get()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('l jS F Y'); 
        });

        return $albums;
    }

    public function getTimelinePhotos() {
        $photos = Photo::orderBy('created_at', 'desc')->get()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('l jS F Y'); //toFormattedDateString();
        });

        return $photos;
    }
}