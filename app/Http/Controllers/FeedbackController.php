<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use App\Http\Resources\FeedbackResource;

class FeedbackController extends Controller
{
    
    /**
     * Display fedaback page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('feedback.index')->with('title', 'Feedbacks');
    }

    /**
     * Display the specified collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {   
        $feedbacks = Feedback::orderBy('created_at','desc')->get();
        return FeedbackResource::collection($feedbacks);
    }

    /**
     * Display the read feedback collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function getViewed() {
        $feedbacks = Feedback::viewed()->orderBy('created_at','desc')->get();
        return FeedbackResource::collection($feedbacks);
    }

    /**
     * Display the unread feedback collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNew() {
        $feedbacks = Feedback::new()->orderBy('created_at','desc')->get();
        return FeedbackResource::collection($feedbacks);
    }

    /**
     * Display the searched feedback collection.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchFeedbacks(Request $request) {
        $query = strtolower($request->input('query'));
        $feedbacks = Feedback::SearchByKeyword($query)->orderBy('created_at','desc')->get();
        return FeedbackResource::collection($feedbacks);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feedback = Feedback::find($id);
        if($feedback->viewed == 0) {
            $feedback->viewed = 1;
            $feedback->save();
        }
        return new FeedbackResource($feedback);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feedback = new Feedback();
        $feedback->name = $request->name;
        $feedback->email = $request->email;
        $feedback->phone_number = $request->phone_number;
        $feedback->message = $request->message;
        $feedback->save();

        return response()->json(['Success, 200']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feedback = Feedback::find($id);
        if ($feedback) {
            $feedback->delete();
        }

        return response()->json(['Success, 200']);
    }
}
