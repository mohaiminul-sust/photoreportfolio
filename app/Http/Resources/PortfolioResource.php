<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;

class PortfolioResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'site_title' => $this->site_title,
            'banner_main_title' => $this->banner_main_title,
            'banner_main_sub' => $this->banner_main_sub,
            'bg_image' => url('/').$this->bg_image,
            'bio' => $this->bio,
            'banner_first_title' => $this->banner_first_title,
            'banner_first_desc' => $this->banner_first_desc,
            'banner_second_title' => $this->banner_second_title,
            'banner_second_desc' => $this->banner_second_desc,
            'intro_header' => $this->intro_header,
            'intro_content' => $this->intro_content,
            'address' => $this->address,
            'phone_number' => $this->phone_number,
            'facebook_link' => $this->facebook_link,
            'flickr_link' => $this->flickr_link,
            'instagram_link' => $this->instagram_link,
            'pexels_link' => $this->pexels_link, 
            'user' => $this->user,
            'created_date' => Carbon::parse($this->created_at)->toDateTimeString(),
            'updated_date' => Carbon::parse($this->updated_at)->toDateTimeString(),
            'created_ago' => Carbon::parse($this->created_at)->diffForHumans(Carbon::now()),
            'updated_ago' => Carbon::parse($this->updated_at)->diffForHumans(Carbon::now())
        ];
    }
}
