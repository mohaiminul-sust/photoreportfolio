<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;

class SinglePhotoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image ? url('/').$this->image : null,
            'caption' => $this->caption,
            'notes' => $this->notes,
            'album' => $this->album,
            'tags' => $this->tags,
            'public' => $this->is_public,
            'created_date' => Carbon::parse($this->created_at)->toDateTimeString(),
            'updated_date' => Carbon::parse($this->updated_at)->toDateTimeString(),
            'created_ago' => Carbon::parse($this->created_at)->diffForHumans(Carbon::now()),
            'updated_ago' => Carbon::parse($this->updated_at)->diffForHumans(Carbon::now())
        ];
    }
}
