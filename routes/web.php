<?php

Route::get('/', function () {
    return redirect()->route('portfolio.index');
});

Route::get('/admin', function () {
    return redirect()->route('home');
});

Auth::routes();
Route::get('/me/update','HomeController@showUpdateForm')->name('updateuserform');
Route::post('/me/update','HomeController@updateUser')->name('updateuser');
 
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'albums'], function() {
    Route::get('/', 'AlbumController@index')->name('album.index');
    Route::get('/all', 'AlbumController@getAlbums')->name('album.all');
    Route::get('/list', 'AlbumController@listAlbums')->name('album.list');
    Route::get('/search', 'AlbumController@searchAlbums')->name('album.search');
    Route::get('/create', 'AlbumController@create')->name('album.create');
    Route::post('/create', 'AlbumController@store')->name('album.store');
    Route::get('/{id}', 'AlbumController@show')->name('album.show');
    Route::get('/update/{id}', 'AlbumController@edit')->name('album.edit');
    Route::post('/update/{id}', 'AlbumController@update')->name('album.update');
    Route::get('/delete/{id}', 'AlbumController@destroy')->name('album.delete');
    Route::get('/preview/{id}', 'AlbumController@preview')->name('album.preview');
    Route::post('/upload/cover/{id}', 'AlbumController@updateCoverImage')->name('album.uploadcover');
});

Route::group(['prefix' => 'photos'], function() {
    Route::get('/', 'PhotoController@index')->name('photo.index');
    Route::get('/all', 'PhotoController@getPhotos')->name('photo.all');
    Route::get('/public', 'PhotoController@getPublicPhotos')->name('photo.public');
    Route::get('/public/all', 'PhotoController@getAllPublicPhotos')->name('photo.allpublic');
    Route::get('/private', 'PhotoController@getPrivatePhotos')->name('photo.private');
    Route::get('/search', 'PhotoController@searchPhotos')->name('photo.search');
    Route::get('/{id}', 'PhotoController@show')->name('photo.show');
    Route::get('/upload/image/new', 'PhotoController@uploadImage')->name('photo.uploadimage');
    Route::get('/upload/image/album/{id}', 'PhotoController@uploadImageByAlbum')->name('photo.uploadimagebyalbum');
    Route::post('/create/{id}', 'PhotoController@store')->name('photo.store');
    Route::get('/update/{id}', 'PhotoController@edit')->name('photo.edit');
    Route::post('/update/{id}', 'PhotoController@update')->name('photo.update');
    Route::get('/update/public/{id}', 'PhotoController@makePublic')->name('photo.makepublic');
    Route::get('/update/private/{id}', 'PhotoController@makePrivate')->name('photo.makeprivate');
    Route::get('/delete/{id}', 'PhotoController@destroy')->name('photo.delete');
    Route::get('/preview/{id}', 'PhotoController@preview')->name('photo.preview');
    Route::post('/upload/image/{id}', 'PhotoController@updateImage')->name('photo.updateimage');
    Route::get('/album/{id}', 'PhotoController@getPhotosByAlbum')->name('photo.byalbum');
    Route::group(['prefix' => 'tags'], function() {
        Route::post('/create', 'TagController@create')->name('photo.tag.create');
        Route::get('/delete/{id}', 'TagController@destroy')->name('photo.tag.delete');
    });
});

Route::group(['prefix' => 'feedbacks'], function() {
    Route::get('/', 'FeedbackController@index')->name('feedback.index')->middleware('auth');
    Route::get('/all', 'FeedbackController@get')->name('feedback.all')->middleware('auth');
    Route::get('/read', 'FeedbackController@getViewed')->name('feedback.read')->middleware('auth');
    Route::get('/new', 'FeedbackController@getNew')->name('feedback.new')->middleware('auth');
    Route::get('/search', 'FeedbackController@searchFeedbacks')->name('feedback.search')->middleware('auth');
    Route::get('/{id}', 'FeedbackController@show')->name('feedback.show')->middleware('auth');
    Route::post('/store', 'FeedbackController@store')->name('feedback.store');
    Route::get('/delete/{id}', 'FeedbackController@destroy')->name('feedback.delete')->middleware('auth');
});

Route::group(['prefix' => 'timeline'], function() {
    Route::get('/album', 'HomeController@albumTimeline')->name('timeline.album');
    Route::get('/photo', 'HomeController@photoTimeline')->name('timeline.photo');
    Route::get('/albums', 'HomeController@getTimelineAlbums')->name('timeline.albums');
    Route::get('/photos', 'HomeController@getTimelinePhotos')->name('timeline.photos');
});

Route::group(['prefix' => 'portfolio'], function() {
    Route::get('/', 'PortfolioController@index')->name('portfolio.index');
    Route::get('/content', 'PortfolioController@getContent')->name('portfolio.content');
    Route::get('/update', 'PortfolioController@edit')->name('portfolio.edit')->middleware('auth');
    Route::post('/update/{id}', 'PortfolioController@update')->name('portfolio.update')->middleware('auth');
    Route::post('/upload/background/{id}', 'PortfolioController@updateBackground')->name('portfolio.uploadbg')->middleware('auth');
});

Route::get('php-info', function() {
    return redirect('/phpinfo');
})->name('php-info');