@extends('layouts.app')

@section('content')
<div class="container">
    <div id="feedback" v-cloak>
        <div class="box">
            <div class="box-header with-border">
                <a href="{{ route('feedback.index') }}">
                    <h3 class="box-title center">Feedbacks</h3>
                </a>
                <span v-if="feedbacks.length > 0" class="description" style="margin-left: 20px">@{{ feedbacks.length }}</span>
                <div class="pull-right" style="padding: 5px;">
                    <el-select
                        v-model="value"
                        placeholder="Filter"
                        :disabled="selectDisabled"
                        @change="fetchFeedbacks">
                        <el-option
                        v-for="item in options"
                        :key="item.value"
                        :label="item.label"
                        :value="item.value">
                        </el-option>
                    </el-select>
                </div>
                <div class="pull-right" style="padding: 5px;">
                    <el-input
                        placeholder="Search Feedbacks"
                        v-model="searchString"
                        v-show="feedbacks.length > 0"
                        v-on:keyup.enter.native="searchFeedbacks">
                        <i slot="prefix" class="el-input__icon el-icon-search"></i>
                    </el-input>
                </div>
            </div>
            <div class="box-body text-center" v-loading="loading">
                <div v-if="feedbacks.length > 0">
                    <el-table
                        :data="feedbacks"
                        :default-sort = "{prop: 'id', order: 'descending'}"
                        stripe
                        style="width: auto; text-align: left;"
                        max-height="590">
                        <el-table-column prop="viewed" label="Status" :formatter="statusFormatter" width="80"></el-table-column>
                        <el-table-column prop="id" label="#" sortable width="100"></el-table-column>
                        <el-table-column prop="name" label="Name" sortable></el-table-column>
                        <el-table-column prop="email" label="Mail Address"></el-table-column>
                        <el-table-column prop="phone_number" label="Phone Number"></el-table-column>
                        <el-table-column prop="created_ago" label="Arrival Time"></el-table-column>
                        <el-table-column prop="created_date" label="Date" sortable></el-table-column>
                        <el-table-column label="Message" width="150">
                            <template slot-scope="scope">
                                <div> 
                                    {{-- style="padding: 15px;" --}}
                                    <el-button :span=1 @click.native.prevent="viewFeedback(scope.row.id)" type="primary" size="mini" icon="el-icon-tickets"></el-button>
                                    <el-button :span=1 @click="deleteFeedback(scope.row.id)" type="danger" size="mini" icon="el-icon-circle-close"></el-button>                                
                                </div>
                            </template>
                        </el-table-column>
                    </el-table>
                    <span class="text-center"><i class="el-icon-arrow-down"></i></span>
                </div>
                <span v-else class="description">No Feedbacks in here!</span>
            </div>
        </div>
        <sweet-modal @close="closeModal" ref="feedbackMessage">
            <div class="box-body" v-cloak>
                <strong><span class="box-header description" style="text-align: left;">Arrived @{{ feedback.created_ago }} from @{{ feedback.email }}</span></strong>
                <small><span class="box-header description" style="text-align: justify;">@{{ feedback.message }}</span></small>
            </div>
        </sweet-modal>
    </div>
</div>
@endsection

@section('script')
    var feedback = new Vue({
        el: '#feedback',
        data: {
            feedbacks: [],
            feedback: {},
            errors: {},
            loading: false,
            options: [
                {
                    value: 'all',
                    label: 'Show All'
                }, 
                {
                    value: 'new',
                    label: 'Only New'
                },
                {
                    value: 'read',
                    label: 'Only Read'
                }
            ],
            value: 'all',
            statusLoad: false,
            searchString: ''
        },
        computed: {
            selectDisabled: function() {
                return this.loading ? true : false;
            }
        },
        created() {
            this.fetchFeedbacks();
        },
        methods: {
            fetchFeedbacks: function() {
                this.searchString = '';
                var link = "{!! route('feedback.all') !!}";
                switch(this.value) {
                    case "all":
                        break;
                    case "new":
                        link = "{!! route('feedback.new') !!}";
                        break;
                    case "read":
                        link = "{!! route('feedback.read') !!}";        
                        break;
                }
                if (this.statusLoad == false) {
                    this.loading = true;
                }
                axios.get(link)
                .then(function (response) {
                    this.feedbacks = response.data.data;
                    if (this.statusLoad == false) {
                        this.loading = false;
                    }
                }.bind(this))
                .catch(function (error) {
                    this.errors = error;
                    this.loading = false;
                });

                if(this.statusLoad == true) {
                    this.statusLoad = false;
                }
            },
            searchFeedbacks: function() {
                let query = this.searchString;
                if (query.length <= 0) {
                    this.$message.warning('Enter something in search box to search');  
                    return;
                }
                var link = "{!! url('feedbacks/search') !!}/?query=" + query;
                console.log("Firing : " + link);
                this.loading = true;
                axios.get(link)
                .then(function (response) {
                    this.feedbacks = response.data.data;
                    this.loading = false;
                }.bind(this))
                .catch(function (error) {
                    this.formerrors = error;
                    this.loading = false;
                }); 
            },
            viewFeedback: function(id) {
                console.log('ID: ' + id);
                var link = "{!! url('feedbacks') !!}/" + id;
                axios.get(link)
                .then(function (response) {
                    this.feedback = response.data.data;
                    this.$refs.feedbackMessage.open();
                }.bind(this))
                .catch(function (error) {
                    this.errors = error;
                });
            },
            deleteFeedback: function(id) {
                var link = "{!! url('feedbacks/delete') !!}/" + id;
                axios.get(link)
                .then(function (response) {
                    this.$message.success('Feedback deleted!');
                    this.fetchFeedbacks();
                }.bind(this))
                .catch(function (error) {
                    this.errors = error;
                });
            },
            statusFormatter: function(row, column) {
                return row.viewed ? "Viewed" : "NEW";
            },
            closeModal: function() {
                this.statusLoad = true;
                this.fetchFeedbacks();
            }
        }
    })
@endsection