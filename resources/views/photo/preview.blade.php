@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/formtags.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div id="preview-photo" class="container" v-cloak>
        <div class="box box-primary">
            <div class="box-header with-border user-block">
                <img v-img class="img-circle img-bordered-sm" :src="photo.album.cover_image" :alt="photo.album.name">
                <span class="username">Photo : from album <button @click="showAlbum" class="btn-sm btn-primary">@{{ photo.album.name }}</button></span>
                <span class="description">Created @{{ photo.created_ago }} at @{{ photo.created_date }}</span>
                <span class="description">Updated at @{{ photo.updated_date }}</span>
                <div class="pull-right">
                    <el-button @click="deletePhoto" type="danger" icon="el-icon-delete"></el-button>
                    <el-button @click="editPhoto" class="pull-right" type="success" icon="el-icon-edit"></el-button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box">
                    <div class="box-header">
                        <strong><i class="fa fa-image margin-r-5"></i> Photo</strong>
                        <button v-show="hasMetaData" @click="openImageInfo" type="button" class="btn-sm btn-primary pull-right"><i class="fa fa-align-left"></i> Info</button>
                    </div>
                    <div class="box-body">
                        <img v-on:load="exifData" v-img class="image-aspect" :src="photo.image" :alt="photo.caption" id="photoimg" ref="photoimg">
                    </div>
                </div>
                <div class="box">
                    <div class="box-header">
                        <strong><i class="fa fa-comment margin-r-5"></i> Caption</strong>
                    </div>
                    <div class="box-body">
                        <span class="description">@{{ photo.caption }}</span>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header">
                        <strong><i class="fa fa-tags margin-r-5"></i> Tags</strong>
                    </div>
                    <div v-if="photo.tags.length == 0" class="box-body text-center">
                        <span class="description">No Tags Given</span>
                    </div>
                    <div v-else class="box-body">
                        <el-tag v-for="tag in photo.tags" :key="tag.id">@{{ tag.tag }}</el-tag>
                    </div>
                </div>
                <div class="box">
                    <div class="box-header">
                        <strong><i class="fa fa-sticky-note margin-r-5"></i> Story</strong>
                    </div>
                    <div v-if="photo.notes == null" class="box-body text-center">
                        <span class="description">No Story Yet</span>
                    </div>
                    <div v-else class="box-body">
                        <span class="description">@{{ photo.notes }}</span>
                    </div>
                </div>
            </div>
        </div>
        <sweet-modal ref="imageinfo">
            <sweet-modal-tab title="Camera" id="cameraTab" v-cloak>
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th class="centered-table-header">Parameter</th>
                                <th class="centered-table-header">Value</th> 
                            </tr>
                            <tr v-for="(value, key) in cameraData">
                                <td>@{{ toCapitalizedWords(key) }}</td>
                                <td>@{{ value }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </sweet-modal-tab>
            <sweet-modal-tab title="Image" id="imageTab" v-cloak>
                <div class="box-body">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th class="centered-table-header">Parameter</th>
                                <th class="centered-table-header">Value</th> 
                            </tr>
                            <tr v-for="(value, key) in imageData">
                                <td>@{{ toCapitalizedWords(key) }}</td>
                                <td>@{{ value }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </sweet-modal-tab>
        </sweet-modal>
    </div>
@endsection

@section('script')
    var previewphoto = new Vue({
        el: '#preview-photo',
        data: {
            photo: {},
            exif: {}
        },
        computed: {
            hasMetaData : function() {
                return !this.hasNoValue(this.cameraData) || !this.hasNoValue(this.imageData);
            },
            cameraData : function() {
                let data = this.exif;
                return {
                    brand: data.Make ? this.toCapitalizedWords(data.Make) : data.Artist ? this.toCapitalizedWords(data.Artist) : 'Not Found',
                    model: data.Model ? data.Model : 'Not Found',
                    aperture: data.FNumber ? data.FNumber : 'Not Found',
                    shutterSpeed: data.ShutterSpeedValue ? data.ShutterSpeedValue : 'Not Found',
                    whiteBalance: data.WhiteBalance ? data.WhiteBalance : 'Not Found',
                    exposure: data.ExposureProgram ? data.ExposureProgram : 'Not Found',
                    exposureTime: data.ExposureTime ? data.ExposureTime : 'Not Found',
                    exposureBias: data.ExposureBias ? data.ExposureBias : 'Not Found',
                    focalLength: data.FocalLength ? data.FocalLength : 'Not Found',
                    iso: data.ISOSpeedRatings ? data.ISOSpeedRatings : 'Not Found',
                    meteringMode: data.MeteringMode ? data.MeteringMode : 'Not Found',
                    flash: data.Flash ? data.Flash : 'Not Found'
                };
            },
            imageData : function() {
                let data = this.exif;
                console.log('Exif Meta : ' + data);
                return {
                    editedBy: data.Software ? data.Software : 'Not Found',
                    width: data.XResolution ? data.XResolution + ' px' : 'Not Found',
                    height: data.YResolution ?  data.YResolution + ' px' : 'Not Found',
                    orientation: this.humanizeOrientation(data.Orientation),
                    color: this.humanizeImageColorMode(data.ColorSpace),
                    originalDate: data.DateTimeOriginal ? data.DateTimeOriginal : 'Not Found'
                };
            }
        },
        created() {
            this.fetchPhoto({!! $id !!});
        },
        methods: {
            hasNoValue : function(obj) {
                for(var i in obj) {
                    if(obj[i] !== 'Not Found')
                    return false;
                }
                return true;
            },
            exifData : function() {
                var img1 = document.getElementById("photoimg");
                const self = this;
                EXIF.getData(img1, function() {
                    var allMetaData = EXIF.getAllTags(this);
                    console.log(allMetaData);
                    self.exif = allMetaData;
                });
            },
            fetchPhoto: function(id) {
                console.log("before fire");
                var link = "{!! url('photos') !!}/" + id;
                console.log("firing " + link)
                axios.get(link)
                .then(function (response) {
                    this.photo = response.data.data;
                    console.log(this.photo.image);
                    this.exifData();
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            deletePhoto: function () {
                var link = "{!! url('photos/delete') !!}/" + this.photo.id;
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    var redirect = "{!! url('photos') !!}";
                    document.location.href = redirect;
                }.bind(this))
                .catch(function (error) {
                    this.formerrors = error;
                });
            },
            editPhoto: function() {
                var link = "{!! url('photos/update') !!}/" + this.photo.id;
                document.location.href = link;
            },
            showAlbum: function() {
                var link = "{!! url('albums/preview') !!}/" + this.photo.album.id;
                document.location.href = link;
            },
            openImageInfo: function() {
                this.$refs.imageinfo.open();
            },
            toCapitalizedWords: function(name) {
                var words = name.match(/[A-Za-z][a-z]*/g);
                return words.map(this.capitalize).join(" ");
            },
            capitalize: function(word) {
                if (word == 'iso') {
                    return 'ISO'
                }
                return word.charAt(0).toUpperCase() + word.substring(1);
            },
            humanizeImageColorMode: function(color) {
                switch(color) {
                    case 0:
                        return 'No'
                    case 1:
                        return 'Yes'
                    default:
                        return 'Not Found'        
                }
            },
            humanizeOrientation: function(orientation) {
                switch(orientation) {
                    case 1:
                        return 'Horizontal'
                    case 2:
                        return 'Mirrored Horizontal'
                    case 3:
                        return 'Rotated 180'
                    case 4:
                        return 'Mirrored Vertical'
                    case 5:
                        return 'Mirrored Horizontal & Rotated 270 CW'
                    case 7:
                        return 'Rotated 90 CW'
                    case 7:
                        return 'Mirrored Horizontal & Rotated 90 CW'
                    case 8:
                        return 'Rotated 270 CW'
                    default:
                        return 'Not Found'                            
                }
            }
        }
    })
@endsection