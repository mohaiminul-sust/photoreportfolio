<div class="w3-banner" :style="bannerImage">
    <div class="wthree-different-dot">
        <div class="w3layouts-header-top">
            <div class="container">
                <div class="w3-header-top-grids">
                    <div class="w3-header-top-left">
                        <p><i class="fas fa-phone-volume" aria-hidden="true"></i> @{{ portfolio.phone_number }}</p>
                    </div>
                    <div class="w3-header-top-right">
                        <div class="agileinfo-social-grids">
                            <ul>
                                <li v-if="portfolio.facebook_link"><a :href="portfolio.facebook_link" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
                                <li v-if="portfolio.flickr_link"><a :href="portfolio.flickr_link" target="_blank"><i class="fab fa-flickr"></i></a></li>
                                <li v-if="portfolio.instagram_link"><a :href="portfolio.instagram_link" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li v-if="portfolio.pexels_link"><a :href="portfolio.pexels_link" target="_blank">PEXELS</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div class="head">
            <div class="container">
                <div class="navbar-top">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                            <div class="navbar-brand logo ">
                            <h1><a href="#"><span>@{{ siteTitleHead }}</span>@{{ siteTitleTail }}</a></h1>
                        </div>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav link-effect-4">
                        <li class="active first-list"><a href="#">Home</a></li>
                        {{--  <li><a href="#about" class="scroll">About</a></li>  --}}
                        <li><a href="#gallery" class="scroll">Gallery</a></li>
                        {{-- <li><a href="#services" class="scroll">Services</a></li> --}}
                        {{--  <li><a href="#team" class="scroll">Team</a></li>  --}}
                        <li><a href="#contact" class="scroll">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner">
            <div class="container">
                <div class="w3ls-icon"><i class="fas fa-camera" aria-hidden="true"></i></div>
                <div class="border"></div>
                <div class="callbacks_container-wrap">
                    <ul class="rslides" id="slider3">
                        <li>
                            <div class="slider-info">
                                <h3>@{{ portfolio.banner_main_title }}</h3>
                                <p>@{{ portfolio.banner_main_sub }}</p>
                                <div class="more-button">
                                    <a href="#" data-toggle="modal" data-target="#myModal">Read Bio</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>