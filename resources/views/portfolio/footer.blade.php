<footer v-cloak>
    <div class="copyright">
        <div class="container">
            <p>© @{{ year }} @{{ portfolio.site_title }}. All rights reserved | Designed by <a href="https://www.linkedin.com/in/mohaiminul-islam-46270b101/" target="_blank">Mohaiminul Islam</a></p>
        </div>
    </div>
</footer>
