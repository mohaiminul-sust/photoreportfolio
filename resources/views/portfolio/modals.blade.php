<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
                    <h4 class="modal-title"><span>@{{siteTitleHead}}</span>@{{siteTitleTail}}</h4>
                </div> 
                <div class="modal-body">
                <div class="agileits-w3layouts-info">
                    <img :src="portfolio.bg_image" :alt="portfolio.banner_main_title"/>
                    <div class="box">
                        <div class="box-body">
                            <span v-if="portfolio.bio" class="bio">@{{portfolio.bio}}</span>
                            <p v-else class="bio text-center">No Bio Yet!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>