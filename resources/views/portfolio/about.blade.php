<div class="about" id="about">
    <div class="container">
        <div class="w3l-heading about-heading">
            <h3>About Us</h3>
            <div class="w3ls-border about-border"> </div>
        </div>
        <div class="agileits-about-grids">
            <div class="about-grids">
                <div class="col-md-6 about-grid">
                    <div class="col-xs-3 about-grid-left">
                        <span class="glyphicon glyphicon-asterisk effect-1" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-9 about-grid-right">
                        <h4>Cum soluta nobis est</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus 
                        reiciendis maiores alias consequatur aut</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-6 about-grid">
                    <div class="col-xs-3 about-grid-left">
                        <span class="glyphicon glyphicon-road effect-1" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-9 about-grid-right">
                        <h4>Soluta nobis est cum</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus 
                        reiciendis maiores alias consequatur aut</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="about-grids1">
                <div class="col-md-6 about-grid">
                    <div class="col-xs-3 about-grid-left">
                        <span class="glyphicon glyphicon-plane effect-1" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-9 about-grid-right">
                        <h4>Eligendi cum soluta nobis</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus 
                        reiciendis maiores alias consequatur aut</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-6 about-grid">
                    <div class="col-xs-3 about-grid-left">
                        <span class="glyphicon glyphicon-bed effect-1" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-9 about-grid-right">
                        <h4>Nobis cum soluta est</h4>
                        <p>Itaque earum rerum hic tenetur a sapiente delectus 
                        reiciendis maiores alias consequatur aut</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</div>