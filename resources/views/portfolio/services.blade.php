<div class="services" id="services">
    <div class="container">
        <div class="w3l-heading">
            <h3>Services</h3>
            <div class="w3ls-border"> </div>
        </div>
        <div class="agileinfo_services_grids">
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid_left">
                    <h4>Donec vel sapien</h4>
                </div>
                <div class="agileinfo_services_grid_right">
                    <h4>01.</h4>
                </div>
                <div class="clearfix"> </div>
                <p>Aliquam ex orci, dapibus eu facilisis non, scelerisque id nisl. 
                    Praesent at tincidunt nisi. Aliquam molestie sem a purus pharetra.</p>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid_left">
                    <h4>Lorem ipsum</h4>
                </div>
                <div class="agileinfo_services_grid_right">
                    <h4>02.</h4>
                </div>
                <div class="clearfix"> </div>
                <p>Aliquam ex orci, dapibus eu facilisis non, scelerisque id nisl. 
                    Praesent at tincidunt nisi. Aliquam molestie sem a purus pharetra.</p>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid_left">
                    <h4>Nullam semper</h4>
                </div>
                <div class="agileinfo_services_grid_right">
                    <h4>03.</h4>
                </div>
                <div class="clearfix"> </div>
                <p>Aliquam ex orci, dapibus eu facilisis non, scelerisque id nisl. 
                    Praesent at tincidunt nisi. Aliquam molestie sem a purus pharetra.</p>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid_left">
                    <h4>Duis tempor</h4>
                </div>
                <div class="agileinfo_services_grid_right">
                    <h4>04.</h4>
                </div>
                <div class="clearfix"> </div>
                <p>Aliquam ex orci, dapibus eu facilisis non, scelerisque id nisl. 
                    Praesent at tincidunt nisi. Aliquam molestie sem a purus pharetra.</p>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid_left">
                    <h4>Nunc non augue</h4>
                </div>
                <div class="agileinfo_services_grid_right">
                    <h4>05.</h4>
                </div>
                <div class="clearfix"> </div>
                <p>Aliquam ex orci, dapibus eu facilisis non, scelerisque id nisl. 
                    Praesent at tincidunt nisi. Aliquam molestie sem a purus pharetra.</p>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid_left">
                    <h4>Aliquam era</h4>
                </div>
                <div class="agileinfo_services_grid_right">
                    <h4>06.</h4>
                </div>
                <div class="clearfix"> </div>
                <p>Aliquam ex orci, dapibus eu facilisis non, scelerisque id nisl. 
                    Praesent at tincidunt nisi. Aliquam molestie sem a purus pharetra.</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>