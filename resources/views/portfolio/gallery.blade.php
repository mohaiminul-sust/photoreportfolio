<div class="gallery" id="gallery">
    <div class="container">
        <div class="w3l-heading">
            <h3>Gallery</h3>
            <div class="w3ls-border"> </div>
        </div>
        {{--  images  --}}
        <div class="cards">
            <div class="card" v-for="photo in photos">
                <progressive-img v-img :src="photo.image" :alt="photo.caption">
            </div>
        </div>
    </div>
</div>