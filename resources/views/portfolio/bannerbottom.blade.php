<div class="banner-bottom">
    <div class="container">
        <div class="banner-bottom-grids">
            <div class="col-md-6 banner-bottom-left">
                <div class="left-border">
                    <div class="left-border-info">
                        <h4>@{{ portfolio.banner_first_title }}</h4>
                        <p>@{{ portfolio.banner_first_desc }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 banner-bottom-left banner-bottom-right">
                <div class="left-border">
                    <div class="left-border-info right-border-info">
                        <h4>@{{ portfolio.banner_second_title }}</h4>
                        <p>@{{ portfolio.banner_second_desc }}</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>