<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('portfolio.head')
<body>
    <div id="portfolio-page" v-cloak>
        @include('portfolio.banner')
        @include('portfolio.bannerbottom')
        @include('portfolio.introduction')
        {{--  @include('portfolio.about')  --}}
        @include('portfolio.gallery')
        {{-- @include('portfolio.services') --}}
        {{--  @include('portfolio.team')  --}}
        {{--  @include('portfolio.subscribe')  --}}
        @include('portfolio.contact')
    
        @include('portfolio.modals')
        @include('portfolio.footer')  
    </div>
    
    @include('portfolio.scripts')

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-90315220-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-90315220-2');
    </script>
</body>	
</html>