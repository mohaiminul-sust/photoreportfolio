@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/customcard.css') }}" rel="stylesheet">
    <link href="{{ asset('css/createalbumform.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div id="update-portfolio" class="container" v-cloak>
        @include('flash::message')
        @if(count($errors) > 0)
        <div class="alert alert-block alert-error fade in" id="error-block">
            <button type="button" class="close"data-dismiss="alert">×</button>
            <h4>Errors Found!</h4>
            <strong>Warning!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="box box-primary" v-loading="loading">
            <div class="box-header with-border">
              <h3 class="box-title center">Update Portfolio Content</h3>
              <button @click="showPortfolio" class="btn-sm btn-primary pull-right">View Portfolio</button>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => ['portfolio.update',  $id]]) !!}
                <div class="box-body">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title center">General</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="SiteName">Site Name</label>
                                <input v-model="portfolio.site_title" class="form-control" placeholder="Enter site name..." name="siteName" type="text">
                            </div>
                            <div class="form-group">
                                <label for="mainTitle">Main Title</label>
                                <input v-model="portfolio.banner_main_title" class="form-control" placeholder="Enter main title..." name="mainTitle" type="text">
                            </div>
                            <div class="form-group">
                                <label for="mainSub">Main Subtitle</label>
                                <input v-model="portfolio.banner_main_sub" class="form-control" placeholder="Enter main subtitle..." name="mainSub" type="text">
                            </div>

                            <div class="form-group">
                                <label for="bio">Bio</label>
                                <textarea v-autosize="portfolio.bio" v-model="portfolio.bio" class="form-control" placeholder="Enter full bio..." name="bio"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title center">Banners Section</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="firstTitle">First Banner Title</label>
                                <input v-model="portfolio.banner_first_title" class="form-control" placeholder="Enter banner title..." name="firstTitle" type="text">
                            </div>
                            <div class="form-group">
                                <label for="firstDesc">First Banner Description</label>
                                <textarea v-autosize="portfolio.banner_first_desc" v-model="portfolio.banner_first_desc" class="form-control" placeholder="Enter banner description ..." name="firstDesc"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="secondTitle">Second Banner Title</label>
                                <input v-model="portfolio.banner_second_title" class="form-control" placeholder="Enter banner title..." name="secondTitle" type="text">
                            </div>
                            <div class="form-group">
                                <label for="secondDesc">Second Banner Description</label>
                                <textarea v-autosize="portfolio.banner_second_desc" v-model="portfolio.banner_second_desc" class="form-control" placeholder="Enter banner description..." name="secondDesc"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title center">Intro Section</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="introTitle">Intro Header</label>
                                <input v-model="portfolio.intro_header" class="form-control" placeholder="Enter intro header title..." name="introTitle" type="text">
                            </div>
                            <div class="form-group">
                                <label for="introContent">Intro Content</label>
                                <textarea v-autosize="portfolio.intro_content" v-model="portfolio.intro_content" class="form-control" placeholder="Enter intro content text..." name="introContent"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title center">Contact Info</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="phone">Phone Number</label>
                                <input v-model="portfolio.phone_number" class="form-control" placeholder="Enter phone number..." name="phone" type="text">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea v-autosize="portfolio.address" v-model="portfolio.address" class="form-control" placeholder="Enter address..." name="address"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title center">Links</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="facebook_link">Facebook</label>
                                <input v-model="portfolio.facebook_link" class="form-control" placeholder="Enter fb link..." name="facebook_link" type="text">
                            </div>
                            <div class="form-group">
                                <label for="flickr_link">Flickr</label>
                                <input v-model="portfolio.flickr_link" class="form-control" placeholder="Enter flickr link..." name="flickr_link" type="text">
                            </div>
                            <div class="form-group">
                                <label for="instagram_link">Instagram</label>
                                <input v-model="portfolio.instagram_link" class="form-control" placeholder="Enter instagram link..." name="instagram_link" type="text">
                            </div>
                            <div class="form-group">
                                <label for="pexels_link">Pexels</label>
                                <input v-model="portfolio.pexels_link" class="form-control" placeholder="Enter pexels link..." name="pexels_link" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Content</button>
                    </div>
                </div>
            {!! Form::close() !!}

            {{--  Background Upload  --}}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title center">Background</h3>
                </div>
                <div class="box-body">
                    <div class="image-container">
                        <el-upload
                        class="avatar-uploader"
                        action="{!! route('portfolio.uploadbg', $id) !!}"
                        :headers="headerInfo"
                        :show-file-list="false"
                        :on-success="handleAvatarSuccess"
                        :before-upload="beforeAvatarUpload">
                        <img v-if="portfolio.bg_image" :src="portfolio.bg_image" class="avatar">
                        <i v-else class="el-icon-plus avatar-uploader-icon-photo"></i>
                        </el-upload>
                        <div class="image-centered-text">Click to upload new background</div>
                    </div>
                </div>
            </div>    

            {{--  Photo selection custom component  --}}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title center">Portfolio Photos</h3>
                </div>
                <div class="box-body">
                    <div v-if="publicPhotos.data.length > 0">
                        <div class="box">
                            <div class="box-header">
                                <span class="description" style="margin-left: 20px">@{{ publicPhotos.meta.from }} - @{{ publicPhotos.meta.to }} of @{{ publicPhotos.meta.total }} photos</span>
                                <div class="pull-right">
                                    <el-button @click="openImagePicker" type="success" icon="el-icon-plus"></el-button>
                                </div>  
                            </div>
                            <div class="box-body">
                                <el-row>
                                    <div class="block text-center">
                                        <el-pagination
                                        layout="prev, pager, next"
                                        :total="publicPhotos.meta.total"
                                        :page-size="publicPhotos.meta.per_page"
                                        :current-page.sync="publicPhotos.meta.current_page"
                                        @current-change="handlePortfolioPhotosPageChange">
                                        </el-pagination>
                                    </div>
                                </el-row>
                                <el-row>
                                    <div class="center">
                                        <el-col class="cardbody" :span="4" v-for="photo in publicPhotos.data" :key="photo">
                                            <el-card :body-style="{ padding: '0px' }">
                                            <div class="parent-card">
                                                <img v-img:group v-bind:src="photo.image" v-bind:alt="photo.caption" class="image" width=200 height=200>
                                            </div>    
                                            <div style="padding: 14px;">
                                                <span>@{{ trimmedText(photo.caption, 17) }}</span>
                                                <div class="bottom clearfix">
                                                <time class="time">
                                                    <i class="el-icon-time"></i>
                                                    <span style="margin-left: 10px">@{{ photo.created_date }}</span>
                                                </time>
                                                <el-button @click="viewPhoto(photo)" class="button" type="primary" icon="el-icon-view"></el-button>
                                                <el-button @click="makePrivate(photo)" class="button pull-right" type="danger" icon="el-icon-close"></el-button>
                                                </div>
                                            </div>
                                            </el-card>
                                        </el-col>
                                    </div>
                                </el-row>
                                <el-row class="box">
                                    <div class="block text-center">
                                        <el-pagination
                                        layout="prev, pager, next"
                                        :total="publicPhotos.meta.total"
                                        :page-size="publicPhotos.meta.per_page"
                                        :current-page="publicPhotos.meta.current_page"
                                        @current-change="handlePortfolioPhotosPageChange">
                                        </el-pagination>
                                    </div>
                                </el-row>
                            </div>
                        </div>
                    </div>
                    <div v-else>
                        <div class="box">
                            <div class="box-header with-border">
                                <div>
                                    <span class="description" style="margin-left: 20px">(0 photos)</span>
                                    <div class="pull-right">
                                        <el-button @click="openImagePicker" type="success" icon="el-icon-plus"></el-button>
                                    </div>  
                                </div>
                            </div>
                            <div class="box-body text-center">
                                <span class="description">
                                    No Photos in portfolio
                                    </br>
                                    Click (+) to add photos in portfolio
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <sweet-modal ref="imagepicker">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title center">Pick Photo</h3>
                </div>
                <div class="box-body">
                    <div v-if="modalPhotos.data.length > 0">
                        <div class="box">
                            <div class="box-header">
                                <span class="description" style="margin-left: 20px">@{{ modalPhotos.meta.from }} - @{{ modalPhotos.meta.to }} of @{{ modalPhotos.meta.total }} photos</span>
                            </div>
                            <div class="box-body">
                                <el-row>
                                    <div class="block text-center">
                                        <el-pagination
                                        layout="prev, pager, next"
                                        :total="modalPhotos.meta.total"
                                        :page-size="modalPhotos.meta.per_page"
                                        :current-page.sync="modalPhotos.meta.current_page"
                                        @current-change="handleModalPhotosPageChange">
                                        </el-pagination>
                                    </div>
                                </el-row>
                                <el-row>
                                    <div class="text-center">
                                        <el-col class="cardbody" :span="6" v-for="photo in modalPhotos.data" :key="photo">
                                            <el-card :body-style="{ padding: '0px' }">
                                            <div class="parent-card">
                                                <img v-img v-bind:src="photo.image" v-bind:alt="photo.caption" class="image" width=200 height=130>
                                            </div>    
                                            <div style="padding: 14px;">
                                                <span>@{{ trimmedText(photo.caption, 9) }}</span>
                                                <div class="bottom clearfix">
                                                <el-button @click="makePublic(photo)" class="button" type="primary" icon="el-icon-check"></el-button>
                                                </div>
                                            </div>
                                            </el-card>
                                        </el-col>
                                    </div>
                                </el-row>
                                <el-row class="box">
                                    <div class="block text-center">
                                        <el-pagination
                                        layout="prev, pager, next"
                                        :total="modalPhotos.meta.total"
                                        :page-size="modalPhotos.meta.per_page"
                                        :current-page="modalPhotos.meta.current_page"
                                        @current-change="handleModalPhotosPageChange">
                                        </el-pagination>
                                    </div>
                                </el-row>
                            </div>
                        </div>
                    </div>
                    <div v-else>
                        <div class="box">
                            <div class="box-header with-border">
                                <div>
                                    <span class="description" style="margin-left: 20px">(0 photos)</span> 
                                </div>
                            </div>
                            <div class="box-body text-center">
                                <span class="description">
                                    No Photos!
                                    </br>
                                    Click <a href="{{ route('photo.uploadimage') }}">here</a> to upload photos first
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </sweet-modal>
    </div>
@endsection

@section('script')
    var updateportfolio = new Vue({
        el: '#update-portfolio',
        data: {
            portfolio: {},
            headerInfo: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, DELETE',
                'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, X-File-Name, X-File-Size, X-File-Type',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            bgImageBlob: '',
            latitude: 0.0,
            longitude: 0.0,
            loading: true,
            publicPhotos: {},
            modalPhotos: {}
        },
        created(){
            this.fetchPortfolioContent();
        },
        methods: {
            fetchPortfolioContent: function() {
                var link = "{!! url('portfolio/content') !!}";
                console.log("firing " + link);
                this.loading = true;
                axios.get(link)
                .then(function (response) {
                    this.portfolio = response.data.data;
                    this.loading = false;
                    this.fetchPortfolioPhotos();
                    this.fetchModalPhotos();
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            fetchPortfolioPhotos: function() {
                var link = "{!! url('photos/public') !!}";
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    this.publicPhotos = response.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            handlePortfolioPhotosPageChange: function(val) {
                var link = "{!! url('photos/public') !!}?page=" + val;
                axios.get(link)
                .then(function (response) {
                    this.publicPhotos = response.data
                }.bind(this))
                .catch(function (error) {
                    this.formerrors = error;
                });
            },
            fetchModalPhotos: function() {
                var link = "{!! url('photos/private') !!}";
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    this.modalPhotos = response.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            handleModalPhotosPageChange: function(val) {
                var link = "{!! url('photos/private') !!}?page=" + val;
                axios.get(link)
                .then(function (response) {
                    this.modalPhotos = response.data
                }.bind(this))
                .catch(function (error) {
                    this.formerrors = error;
                });
            },
            makePublic: function(photo) {
                var link = "{!! url('photos/update/public/') !!}/" + photo.id;
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    this.$message.success('Image added to portfolio!');
                    this.fetchModalPhotos();
                    this.fetchPortfolioPhotos();
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            makePrivate: function(photo) {
                var link = "{!! url('photos/update/private/') !!}/" + photo.id;
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    this.$message.warning('Image removed from portfolio!');
                    this.fetchPortfolioPhotos();
                    this.fetchModalPhotos();
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            showPortfolio: function() {
                var link = "{!! route('portfolio.index') !!}";
                document.location.href = link;
            },
            handleAvatarSuccess(res, file) {
                this.bgImageBlob = URL.createObjectURL(file.raw);
                this.portfolio = res.data;
                this.$message.success('Background Image updated!');
            },
            beforeAvatarUpload(file) {
                const isJPG = file.type === 'image/jpeg';
                const isLt2M = file.size / 1024 / 1024 < 20;
        
                if (!isJPG) {
                    this.$message.error('Photo must be JPG format!');
                }
                if (!isLt2M) {
                    this.$message.error('Photo size can not exceed 20MB!');
                }
                return isJPG && isLt2M;
            },
            viewPhoto: function(photo) {
                var link = "{!! url('photos/preview') !!}/" + photo.id;
                document.location.href = link;
            },
            trimmedText: function(text, chars) {
                if(text == null) {
                    return "";
                }
                return text.length > chars ? text.substring(0, chars) + '...' : text;
            },
            openImagePicker: function() {
                this.$refs.imagepicker.open();
            }
        }
    });
@endsection