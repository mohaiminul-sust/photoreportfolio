<div class="introduction" id="intro">
    <div class="container">
        <div class="w3l-heading">
            <h2>@{{ portfolio.intro_header }}</h2>
            <div class="w3ls-border"> </div>
        </div>
        <div class="introduction-info">
            <p>@{{ portfolio.intro_content }}</p>
        </div>
    </div>
</div>