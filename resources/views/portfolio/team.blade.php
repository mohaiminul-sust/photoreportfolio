<div class="team jarallax" id="team">
    <div class="container">
        <div class="w3l-heading about-heading">
            <h3>Our Team</h3>
            <div class="w3ls-border about-border"> </div>
        </div>
        <div class="agile_team_grids">
            <div class="col-md-3 agile_team_grid">
                <div class="view w3-agile-view">
                    <img src="images/t1.jpg" alt=" " class="img-responsive" />
                    <div class="w3lmask">
                        <h5>quis nostrud</h5>
                        <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 agile_team_grid">
                <div class="view w3-agile-view">
                    <img src="images/t2.jpg" alt=" " class="img-responsive" />
                    <div class="w3lmask">
                        <h5>quis nostrud</h5>
                        <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 agile_team_grid">
                <div class="view w3-agile-view">
                    <img src="images/t3.jpg" alt=" " class="img-responsive" />
                    <div class="w3lmask">
                        <h5>quis nostrud</h5>
                        <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 agile_team_grid">
                <div class="view w3-agile-view">
                    <img src="images/t4.jpg" alt=" " class="img-responsive" />
                    <div class="w3lmask">
                        <h5>quis nostrud</h5>
                        <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit.</p>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>