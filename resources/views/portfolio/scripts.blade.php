<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $().UItoTop({ easingType: 'easeOutQuart' });                        
    });
</script>
<script type="text/javascript">
    var portfoliopage = new Vue({
        el: '#portfolio-page',
        data: {
            portfolio: {},
            photos: {},
            feedback: {
                name: '',
                email: '',
                phone: '',
                message: ''
            }
        },
        computed: {
            siteTitleHead : function() {
                let length = this.portfolio.site_title ? this.portfolio.site_title.length : 0;
                if (length == 0) {
                    return '';
                }
                let title = this.portfolio.site_title;
                return title.substring(0, 1);
            },
            siteTitleTail : function() {
                let length = this.portfolio.site_title ? this.portfolio.site_title.length : 0;
                if (length == 0) {
                    return '';
                }
                let title = this.portfolio.site_title;
                return title.substring(1, length);
            },
            bannerImage: function() {
                return this.portfolio.bg_image ? "background-image: url(" + this.portfolio.bg_image + ")" : "background-image: none";
            },
            feedbackempty: function() {
                return this.feedback.name == '' || this.feedback.email == '' || this.feedback.phone == '' || this.feedback.message == '';
            },
            year: function() {
                return new Date().getYear() + 1900;
            }
        },
        created() {
            this.fetchPortfolioContent();
        },
        methods: {
            fetchPortfolioContent: function() {
                var link = "{!! url('portfolio/content') !!}";
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    this.portfolio = response.data.data;
                    this.fetchPortfolioPhotos();
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            fetchPortfolioPhotos: function() {
                var link = "{!! url('photos/public/all') !!}";
                console.log("firing " + link);
                axios.get(link)
                .then(function (response) {
                    this.photos = response.data.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            },
            sendFeedback: function() {
                if (this.feedbackempty == true) {
                    this.$message.warning('Please fill in all the fields before sending feedback');
                    return;
                }

                var link = "{!! route('feedback.store') !!}";
                var payload = {
                    name: this.feedback.name,
                    email: this.feedback.email,
                    phone_number: this.feedback.phone,
                    message: this.feedback.message
                };
                console.log("firing " + link);
                console.log(payload);

                axios.post(link, payload)
                .then(function (response) {
                    this.$message.success('Thank You! Your feedback has been received.');
                    this.feedback = {
                        name: '',
                        email: '',
                        phone: '',
                        message: ''
                    };
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
            }
        }
    })
</script>