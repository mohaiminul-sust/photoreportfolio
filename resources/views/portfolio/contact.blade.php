<div id="contact" class="contact">
    <div class="container"> 
        <div class="w3l-heading">
            <h3>Contact</h3>
            <div class="w3ls-border"> </div>
        </div>
        <div class="contact-row agileits-w3layouts">
            <div class="col-md-6 col-sm-6 contact-w3lsleft">
                <div class="contact-grid agileits">
                    <h4>DROP A LINE</h4>
                    <input v-model="feedback.name" type="text" name="name" placeholder="Name">
                    <input v-model="feedback.email" type="email" name="email" placeholder="Email"> 
                    <input v-model="feedback.phone" type="text" name="phone_number" placeholder="Phone Number">
                    <textarea v-model="feedback.message" name="message" placeholder="Message..."></textarea>
                    <input @click="sendFeedback" type="submit" value="Send Feedback">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 contact-w3lsright">
                <h6>Please give your feedback or contact if you need to book an assignment</h6>
                <div class="address-row">
                    <div class="col-xs-2 address-left">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-10 address-right">
                        <h5>Visit</h5>
                        <p>@{{ portfolio.address }}</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="address-row w3-agileits">
                    <div class="col-xs-2 address-left">
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-10 address-right">
                        <h5>Mail</h5>
                        <p><a href="mailto:info@example.com"> @{{ portfolio.user.email }}</a></p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="address-row">
                    <div class="col-xs-2 address-left">
                        <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-10 address-right">
                        <h5>Call</h5>
                        <p>@{{ portfolio.phone_number }}</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>  
                <!-- map -->
                <div class="map agileits">
                    <iframe src="http://maps.google.com/maps?q=24.909120, 91.833263&z=15&output=embed"></iframe>
                </div>
                <!-- //map --> 
            </div>
            <div class="clearfix"> </div>
        </div>	
    </div>	
</div>