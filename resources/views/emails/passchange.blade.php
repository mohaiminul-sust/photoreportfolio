<!DOCTYPE html>
<html>
<head>
<title>Password Changed</title>
</head>
<body>
<h2>Password successfully changed for {{ $user->name }}</h2>
<br/>
Your registered email-id is {{ $user->email }}. Your new password is <strong>{{ $pass }}</strong>
</body>
</html>
