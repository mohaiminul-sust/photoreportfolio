<!DOCTYPE html>
<html>
<head>
<title>Welcome Email</title>
</head>
<body>
<h2>{{ $user->name }}, welcome to the photomanager and portfolio management system.</h2>
<p> You have been successfully registered. </p>
<br/>
Your registered email-id is {{ $user->email }}. Your password is <strong>{{ $pass }}</strong>. Please, save this mail for future reference.
</body>
</html>
