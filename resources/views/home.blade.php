@extends('layouts.app')

@section('content')
<div class="container">
    <div id="home" v-cloak>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Home</div>
                    <div class="panel-body">
                        Welcome, @{{ user.name }}. You are logged in!
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <a href="{{ route('album.index') }}">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-ash"><i class="ion ion-disc"></i></span>
            
                        <div class="info-box-content">
                        <span class="info-box-text">Albums</span>
                        <span class="info-box-number">@{{ albumsCount }}</span>
                        </div>
                    </div>
                </div>
            </a>
            <a href="{{ route('photo.index') }}">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-black"><i class="ion ion-cube"></i></span>
            
                        <div class="info-box-content">
                            <span class="info-box-text">Photos</span>
                            <span class="info-box-number">@{{ photosCount }}</span>
                        </div>
                    </div>
                </div>
            </a>
            <a href="{{ route('feedback.index') }}">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="ion ion-chatboxes"></i></span>
                        <div class="info-box-content">
                            <el-badge :value="newFeed" class="item">
                                <span class="info-box-text">Feedbacks</span>
                            </el-badge>
                            <span class="info-box-number">@{{ feedbacksCount }}</span>
                        </div>                               
                    </div>
                </div>
            </a>
            <a href="{{ route('portfolio.index') }}">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="ion ion-earth"></i></span>
        
                        <div class="info-box-content">
                        <span class="info-box-text">Portfolio</span>
                        <a href="{{ route('portfolio.edit') }}" ><span class="info-box-number"><small>Manage portfolio</small></span></a>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
@endsection

@section('script')
    var home = new Vue({
        el: '#home',
        data: {
            loadingDate: new Date().toLocaleString(),
            albumsCount: {!! $albumsCount !!},
            photosCount: {!! $photosCount !!},
            feedbacksCount: {!! $feedbacksCount !!},
            newFeed: {!! $newFeedbackCount !!} > 0 ? {!! $newFeedbackCount !!} + ' NEW!' : '',
            user: {!! json_encode($adminUser->toArray()) !!}
        }
    })
@endsection