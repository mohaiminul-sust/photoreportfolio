require('./bootstrap');
require('./easing');
require('./jarallax');
require('./move-top');
require('./SmoothScroll.min');

import ElementUI from 'element-ui';
import VueImg from 'v-img';
import SweetModal from 'sweet-modal-vue/src/plugin.js';
import VueProgressiveImage from 'vue-progressive-image';

window.Vue = require('vue');

Vue.use(ElementUI);
Vue.use(VueProgressiveImage);

const vueImgConfig = {
    altAsTitle: true,
    sourceButton: false,
    openOn: 'click',
    thumbnails: true,
  }
Vue.use(VueImg, vueImgConfig);

Vue.use(SweetModal);

var VueAutosize = require('vue-autosize');
Vue.use(VueAutosize);

// Uncomment following when release
// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;