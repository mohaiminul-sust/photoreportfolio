# PhotoREportfolio 
Laravel 5.5 based system implementing Vue.js 2 for **photographers** to upload, manage their  photos in a remote **photo repository** and organise **portfolio**.

## Description
This project is a full fledged *photo storage and portfolio maintenance service* made for photographers, while taking valuable inputs from professional photographers. It comprises of photo and album organising, searching and storage management related admin controls along with upload timeline view. 

It also has a fast and sophisticated public feedback manager consisting basic operations along with sort, filter and search. It also has separate organising options for portfolio section.

Public can visit the portfolio section to view the photographers portfolio and leave feedbacks only. Only the photographer with an admin access has all the privileges.

## Stacks Used:
1. Laravel 5.5 (PHP 7.1)
2. MySQL 5.6 (DB)
3. Vue.js 2 (Javascript framework)
4. HTML5 & CSS3 (custom + bootstrap + element-ui) with media query support for mobile devices
5. Customized Icon and style classes built in SASS (dynamic CSS)
6. Webpack (Build Tool) for asset optimization and building dynamic resources throughout the project. Can be used to minify assets upon production launch.

**This project is fully integratable with fly-systems for storing massive amount of photos remotely e.g. Amazon AWS, RackSpace, Digital Ocean's Fileserver etc.

## How to use
1. Clone the repo

2. Create a database in your mysql database.

3. Edit the **.env** file's following section to set your machine's mysql's port, db_name, username & pass (I was using MAMP)
```
DB_PORT=8889
DB_DATABASE=photomgr
DB_USERNAME=root
DB_PASSWORD=root
```
4. Run **composer install** (You've to install composer if you didn't install it prior) [ To install php dependencies ]
5. Run **npm install** (You've to install npm if you didn't install it prior) [ To install javascript dependencies ]
6. Run **npm run dev** [ To build js, css, image, font, icons for dev deploy ]
7. Run **php artisan migrate --seed** [ To migrate the db structure to database + populate with primary data]
8. Run **php artisan serve** [ Runs the server in 'localhost:8000' ]

9. Hit *localhost:8000* via browser to show home page.
10. If you're not logged in you'll be prompted for login upon reaching the site. Use the following email -

> mohaiminul.sust@gmail.com

and password,
 
> secretadmin

**you can also create a new account to access the site.

## License
Basically, feel free to use and re-use any way you want. Just do it with prper credits where due :)
